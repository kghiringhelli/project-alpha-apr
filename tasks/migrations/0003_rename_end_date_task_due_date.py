# Generated by Django 4.1.7 on 2023-03-07 00:18

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("tasks", "0002_rename_taskmodel_task"),
    ]

    operations = [
        migrations.RenameField(
            model_name="task",
            old_name="end_date",
            new_name="due_date",
        ),
    ]
